// [Section] comparison Query Operators

// $gt$gte operator

	/*
		-it allows us to find document that have field number value greater than or equal to a specified value
		-syntax:
		db.collectionName.find({field:{$gt:value}});
		db.collectionName.find({field:{$gte:value}});
	*/
	// $gt
	db.users.find({age:{$gt:50}}).pretty();

	// $gte
	db.users.find({age:{$gte:21}}).pretty();

// $lt/$lte operator
	/*
	-it allows us to find/retrieve document that have field number value less than or equal to a specified value
		-syntax:
		db.collectionName.find({field:{$lt:value}});
		db.collectionName.find({field:{$lte:value}});
	
	*/
	// $lt
	db.users.find({age:{$lt:50}}).pretty();
	// $lte
	db.users.find({age:{$lte:76}}).pretty();

// $ne operator
	/*
		-allows us to find documents that have field number values not equal to a specified value
		syntax:
			db.collectionName.find({field:{$ne:value}});
	*/
	db.users.find({age:{$ne:68}}).pretty();

// $in operator 
	/*
		-it allows us to find coduments.document with specific match criteria one field using different values.
		syntax:
			db.collectionName.find({field:{$in:[valueA,valueB]}});
	*/
	db.users.find({lastName:{$in:["Doe","Hawking"]}});

	db.users.find({courses:{$in:["HTML","React"]}});

	db.users.find({courses:{$in:["CSS","JavaScript"]}});

// [Section] logical Query Operators
	// $or operator
	/*
		-Allows us to find documents that match a single criteria from multiple provided search criteria.
		syntax:
			db.collectionName.find({$or:[{fieldA:valueA},{fieldB:valueB}]});
	*/

	db.users.find({$or:[{firstName:"Neil"},{age:25}]});

	db.users.find({$or:[{firstName:"Neil"},{age:{$gt:30}}]});

	// $and operator

	/*
		-allows us to find documents matching multiple criteria in a multiple field.
			syntax:
			db.collectionName.find({$and:[{fieldA:valueA},{fieldB:valueB}]});
	*/

	db.users.find({$and:[{firstName:"Neil"},{age:{$gt:30}}]});

	db.users.find({$and:[{firstName:"Neil"},{age:25}]});

	// [Section] Field projection

	/*
		retrieving documents are common operations that we do and by default mongoDB query return the whole document as a response
	*/

	// inclusion
		/*
			-it allows us to include/add specific fields only when retrieving documents.
			Syntax
				db.users.find({criteria},{fields:1});
		*/

	db.users.find({firstName:"Jane"},
			{
				firstName:1,
				lastName:1,
				age:1
			}
		);

	db.users.find({$and:[{firstName:"Neil"},{age:{$gt:30}}]},
			{
				firstName:1,
				lastName:1,
				age:1
			}
		);

	// Exclusion

	/*
		-allows us to exclud/remove specific fields only when retrieving documents.
		-The value provided is 0.
	*/

	db.users.find({firstName:"Jane"},
	{
		contact:0,
		department:0
	});

	db.users.find({$and:[{firstName:"Neil"},{age:{$gt:30}}]},
			{
				age:false,
				courses:0
			}
		);

	// Returning Specific fields in Embedded documents

	db.users.find({firstName:"Jane"},
	{
		"contact.phone":1,
		_id:0
	});

	db.users.find({firstName:"Jane"},
	{
		"contact.phone":0,
	});

//[Section] Evaluation Query Operators
	/*
		-it allows us to find documents that match a specific string pattern using regular expressions
		syntax:
			db.users.find({feild:$regex:'pattern',$options:'$optionValue'})
	*/

	// case sensitive query
	db.users.find({firstName:{$regex:'N',$options:'$i'}});


	//CRUD operations

	db.users.updateOne({age:{$lte:17}},{
		$set:{
			firstName:"Chris",
			lastName:"Mortel"
		}
	});

	db.users.deleteOne({$and:[{firstName:"Chris"},{lastName:"Mortel"}]});